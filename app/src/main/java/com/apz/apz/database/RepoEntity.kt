package com.apz.apz.database

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.apz.apz.database.RepoEntity.Companion.MAIN_TABLE_NAME

@Entity(tableName = MAIN_TABLE_NAME)
data class RepoEntity (
        val repoName: String?,
        val userName: String?,
        val avatarLink: String?,
        val description: String?,
        val bitbucket: Boolean = false
) {

    @PrimaryKey(autoGenerate = true) var id: Int? = null

    companion object {
        const val MAIN_TABLE_NAME = "Repos"
        const val COLUMN_ID = "id"
    }
}
