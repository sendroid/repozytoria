package com.apz.apz.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.apz.apz.database.RepoEntity.Companion.MAIN_TABLE_NAME
import io.reactivex.Flowable

@Dao
interface RepoDao {

    @Query("SELECT * FROM $MAIN_TABLE_NAME ORDER BY ${RepoEntity.COLUMN_ID}")
    fun listenForRepos(): Flowable<List<RepoEntity>>

    @Query("DELETE FROM $MAIN_TABLE_NAME")
    fun clearTable()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRepos(pages: List<RepoEntity>)

    @Query("SELECT * FROM $MAIN_TABLE_NAME")
    fun count() : Int
}