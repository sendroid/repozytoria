package com.apz.apz.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters

@TypeConverters(Converters::class)
@Database(entities = [RepoEntity::class], version = 1, exportSchema = false)
abstract class DatabaseManager: RoomDatabase(){
    abstract val repoDao: RepoDao
}