package com.apz.apz

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.apz.apz.database.RepoEntity
import com.apz.apz.repository.Repository
import io.reactivex.rxkotlin.subscribeBy

class ReposViewModel(private val repository: Repository) : ViewModel() {

    private val errorMessage = MutableLiveData<String>()

    fun observeRepos(onNext: (List<RepoEntity>) -> Unit, onError: (Throwable) -> Unit)
            = repository.listenForRepos().subscribeBy (onNext = onNext, onError = onError)

    fun observeErrors() : LiveData<String> = errorMessage

    fun redownloadRepos() = repository.downloadRepos(onError = {
        errorMessage.postValue("Wystąpił problem z połączeniem. Spróbuj ponownie.")
    }, onComplete = {
        errorMessage.postValue(null)
    })
}