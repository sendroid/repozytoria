package com.apz.apz.repository

import io.reactivex.rxkotlin.subscribeBy

class Repository(private val taskManager: TaskManager) {

    fun downloadRepos(onError : (Throwable) -> Unit = {}, onComplete: () -> Unit = {})
            = taskManager.downloadRepos().subscribeBy(onError = onError, onComplete = onComplete)

    fun listenForRepos() = taskManager.getAllRepos()

}