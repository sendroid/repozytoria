package com.apz.apz.repository

import com.apz.apz.convertToRepoEntity
import com.apz.apz.database.RepoDao
import com.apz.apz.database.RepoEntity
import com.apz.apz.remote.ApiService
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class TaskManager(private val apiService: ApiService, private val repoDao: RepoDao) {

    fun downloadRepos(): Completable = dbClearCompletable.andThen(downloadFromGithub()).andThen(downloadFromBibucket())

    fun getAllRepos() = repoDao.listenForRepos()
            .observeOn(AndroidSchedulers.mainThread())
            .replay(1)
            .autoConnect(1)

    private val dbClearCompletable = Completable.create {
        repoDao.clearTable()
        it.onComplete()
    }

    private fun downloadFromBibucket(): Completable {
        return Completable.create { emitter ->
            apiService.fetchBitbucket()
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribeBy(
                            onSuccess = { page ->
                                val repos = mutableListOf<RepoEntity>()
                                page.repos.mapTo(repos, transform = { it.convertToRepoEntity() })
                                repoDao.insertRepos(repos)
                                emitter.onComplete()
                            },
                            onError = { throwable ->
                                emitter.onError(throwable)
                            }
                    )
        }
    }

    private fun downloadFromGithub(): Completable {
        return Completable.create { emitter ->
            apiService.fetchGithub()
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribeBy(
                            onSuccess = { list ->
                                val repos = mutableListOf<RepoEntity>()
                                list.mapTo(repos, transform = { it.convertToRepoEntity() })
                                repoDao.insertRepos(repos)
                                emitter.onComplete()
                            },
                            onError = { throwable ->
                                emitter.onError(throwable)
                            }
                    )
        }
    }

}