package com.apz.apz.remote.models

import com.google.gson.annotations.SerializedName

data class BitbucketModel (
        @SerializedName("name")val repoName : String,
        @SerializedName("description")val description : String,
        @SerializedName("owner")val owner : BitbucketOwner
)