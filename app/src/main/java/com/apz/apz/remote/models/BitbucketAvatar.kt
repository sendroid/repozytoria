package com.apz.apz.remote.models

import com.google.gson.annotations.SerializedName

data class BitbucketAvatar (@SerializedName("href")val avatarLink : String)