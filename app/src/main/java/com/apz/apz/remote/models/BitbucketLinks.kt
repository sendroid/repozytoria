package com.apz.apz.remote.models

import com.google.gson.annotations.SerializedName

data class BitbucketLinks (@SerializedName("avatar")val avatarLinks : BitbucketAvatar)