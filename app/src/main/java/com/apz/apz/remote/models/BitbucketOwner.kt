package com.apz.apz.remote.models

import com.google.gson.annotations.SerializedName

data class BitbucketOwner (
        @SerializedName("username")val userName : String,
        @SerializedName("links")val links: BitbucketLinks
)