package com.apz.apz.remote.models

import com.google.gson.annotations.SerializedName

data class BitbucketPage (@SerializedName("values")val repos : List<BitbucketModel>)