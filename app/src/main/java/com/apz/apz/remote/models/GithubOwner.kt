package com.apz.apz.remote.models

import com.google.gson.annotations.SerializedName

data class GithubOwner (
        @SerializedName("login")val userName : String,
        @SerializedName("avatar_url")val avatarLink : String
)