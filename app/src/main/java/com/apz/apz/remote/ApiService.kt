package com.apz.apz.remote

import com.apz.apz.remote.models.BitbucketPage
import com.apz.apz.remote.models.GithubModel
import io.reactivex.Single
import retrofit2.http.GET


interface ApiService {

    companion object {
        const val BASE_URL = "https://api.github.com/repositories/"
        const val BITBUCKET_URL = "https://api.bitbucket.org/2.0/repositories?fields=values.name,values.owner,values.description"
        const val GITHUB_URL = "https://api.github.com/repositories"
    }

    @GET(BITBUCKET_URL)
    fun fetchBitbucket() : Single<BitbucketPage>


    @GET(GITHUB_URL)
    fun fetchGithub() : Single<List<GithubModel>>

}