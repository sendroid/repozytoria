package com.apz.apz.view

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.apz.apz.*
import com.apz.apz.database.RepoEntity
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item.*
import java.util.*

class ReposAdapter : RecyclerView.Adapter<ReposAdapter.ViewHolder>() {

    private var reposList: List<RepoEntity> = Collections.emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.item))

    override fun getItemCount(): Int = reposList.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(reposList[position])
    }

    fun updateRepos(repos: List<RepoEntity>?, sort: Boolean) {
        var repoViews : List<RepoEntity>? = null
        repos?.let {
            repoViews = if(sort) repos.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) {it.repoName!!})
            else repos.sortedWith(compareBy {it.id})
        }
        repoViews?.let {
            notifyChanges(reposList, repoViews!!)
        }
        this.reposList = repoViews ?: Collections.emptyList()
    }

    fun sort(sort : Boolean) {
        updateRepos(reposList, sort)
        notifyDataSetChanged()
    }


    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(repo: RepoEntity) {
            GlideApp.with(itemView)
                    .load(repo.avatarLink?: R.drawable.ic_launcher_background)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .centerCrop()
                    .placeholder(getCircularProgressDrawable(containerView.context))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(avatar)
            repoName.text = repo.repoName
            userName.text = repo.userName
            containerView.setOnClickListener {
                it.context.startDetailsActivity(repo.repoName, repo.userName, repo.avatarLink, repo.description)
            }
            if(repo.bitbucket){
                layoutCard.setBackgroundColor(containerView.resources.getColor(R.color.gold))
            } else {
                layoutCard.setBackgroundColor(containerView.resources.getColor(R.color.white))
            }
        }
    }

    private fun notifyChanges(oldList: List<RepoEntity>, newList: List<RepoEntity>) {
        val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return oldList[oldItemPosition].id == newList[newItemPosition].id
            }
            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return oldList[oldItemPosition] == newList[newItemPosition]
            }
            override fun getOldListSize() = oldList.size
            override fun getNewListSize() = newList.size
        })
        if(oldList.size >= newList.size){
            notifyDataSetChanged()
            return
        }
        diff.dispatchUpdatesTo(this)
    }
}