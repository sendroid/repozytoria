package com.apz.apz.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.apz.apz.GlideApp
import com.apz.apz.R
import com.apz.apz.getCircularProgressDrawable
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {

    companion object {
        val REPO_NAME = "REPO_NAME"
        val USER_NAME = "USER_NAME"
        val AVATAR_LINK = "AVATAR_LINK"
        val DESCRIPTION = "DESCRIPTION"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        title = intent.getStringExtra(REPO_NAME)
        tvUser.text = intent.getStringExtra(USER_NAME)
        tvDescription.text = intent.getStringExtra(DESCRIPTION)
        GlideApp.with(this)
                .load(intent.getStringExtra(AVATAR_LINK) ?: R.drawable.ic_launcher_background)
                .transition(DrawableTransitionOptions.withCrossFade())
                .fitCenter()
                .placeholder(getCircularProgressDrawable(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivAvatar)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
