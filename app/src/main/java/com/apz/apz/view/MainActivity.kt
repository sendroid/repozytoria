package com.apz.apz.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import android.widget.Toast
import com.apz.apz.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private lateinit var prefs : SharedPreferences
    private lateinit var reposViewModel: ReposViewModel
    private lateinit var reposAdapter: ReposAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val actionBar = supportActionBar
        actionBar!!.setHomeButtonEnabled(true)
        val app = applicationContext as App
        prefs = PreferenceHelper.defaultPrefs(app)
        reposViewModel = ViewModelProviders.of(this, ReposViewModelFactory(app.repository)).get(ReposViewModel::class.java)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        reposAdapter = ReposAdapter()
        recyclerView.adapter = reposAdapter
        compositeDisposable += reposViewModel.observeRepos(onNext = {
            reposAdapter.updateRepos(it, prefs.getBoolean(PreferenceHelper.SORT_PREF, false))
            swipeToRefresh.isRefreshing = false
        }, onError = {
            swipeToRefresh.isRefreshing = false
        })

        swipeToRefresh.setOnRefreshListener {
            compositeDisposable += reposViewModel.redownloadRepos()
        }

        reposViewModel.observeErrors().observe(this, Observer {
            swipeToRefresh.isRefreshing = false
            it?.let { Toast.makeText(this, it, Toast.LENGTH_SHORT).show() }
        })


        sortCheckbox.isChecked = prefs.getBoolean(PreferenceHelper.SORT_PREF, false)
        sortLayout.setOnClickListener {
            sortCheckbox.isChecked = !sortCheckbox.isChecked
            prefs.edit{ editor -> editor.putBoolean(PreferenceHelper.SORT_PREF, sortCheckbox.isChecked) }
            reposAdapter.sort(sortCheckbox.isChecked)
        }

    }



    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
