package com.apz.apz

import android.app.Application
import android.arch.persistence.room.Room
import com.apz.apz.database.DatabaseManager
import com.apz.apz.database.RepoDao
import com.apz.apz.remote.ApiService
import com.apz.apz.repository.Repository
import com.apz.apz.repository.TaskManager
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class App: Application() {

    companion object {
        const val DATABASE_NAME = "database"
    }

    private lateinit var databaseManager: DatabaseManager

    lateinit var repoDao: RepoDao
        private set

    lateinit var apiService: ApiService
        private set

    lateinit var repository: Repository
        private set

    lateinit var taskManager: TaskManager
        private set

    override fun onCreate() {
        super.onCreate()
        databaseManager = Room.databaseBuilder(this, DatabaseManager::class.java, DATABASE_NAME)
                .allowMainThreadQueries()
                .build()
        repoDao = databaseManager.repoDao
        apiService = Retrofit.Builder()
                .baseUrl(ApiService.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(ApiService::class.java)
        taskManager = TaskManager(apiService, repoDao)
        repository = Repository(taskManager)
        repository.downloadRepos()

    }
}