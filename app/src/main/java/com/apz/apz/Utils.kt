package com.apz.apz

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.annotation.LayoutRes
import android.support.v4.content.ContextCompat
import android.support.v4.widget.CircularProgressDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apz.apz.database.RepoEntity
import com.apz.apz.remote.models.BitbucketModel
import com.apz.apz.remote.models.GithubModel
import com.apz.apz.repository.Repository
import com.apz.apz.view.DetailsActivity

fun BitbucketModel.convertToRepoEntity() = RepoEntity(repoName, owner.userName, owner.links.avatarLinks.avatarLink, description, true)
fun GithubModel.convertToRepoEntity() = RepoEntity(repoName, owner.userName, owner.avatarLink, description)

/**
 * View inflating
 */
fun ViewGroup.inflate(@LayoutRes layout: Int, attachToRoot: Boolean = false): View =
        LayoutInflater.from(this.context).inflate(layout, this, attachToRoot)

fun Context.startDetailsActivity(repoName: String?, userName: String?, avatarLink: String?, description: String?){
    val intent = Intent(this, DetailsActivity::class.java)
    intent.putExtra(DetailsActivity.REPO_NAME, repoName)
    intent.putExtra(DetailsActivity.USER_NAME, userName)
    intent.putExtra(DetailsActivity.AVATAR_LINK, avatarLink)
    intent.putExtra(DetailsActivity.DESCRIPTION, description)
    startActivity(intent)
}

/**
 * ViewModel initialization
 */
@Suppress("UNCHECKED_CAST")
class ReposViewModelFactory(private val repository: Repository) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ReposViewModel(repository) as T
    }
}

/**
 * SharedPreferences
 */
inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
    val editor = this.edit()
    operation(editor)
    editor.apply()
}


/**
 * Progress in Glide
 */
fun getCircularProgressDrawable(context: Context) = with(CircularProgressDrawable(context)) {
    setColorSchemeColors(ContextCompat.getColor(context, R.color.white_dirty))
    strokeWidth = 10f
    centerRadius = 50f
    start()
    this
}
